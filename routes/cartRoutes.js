const express = require('express')
const cartController = require('./../Controllers/cartController')

const router = express.Router()


router
    .route('/')
    .get(cartController.getAllCart)
    .post(cartController.createCart)

router
    .route('/:id')
    .patch(cartController.updateCart)
    .delete(cartController.deleteAllCart)

module.exports = router