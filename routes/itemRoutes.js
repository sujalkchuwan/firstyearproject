const express = require('express')
const itemController = require('./../Controllers/itemController')

const router = express.Router()

// router.post('/itmForm', itemController.uploadItemPhoto,itemController.createItems)

router
    .route('/')
    .get(itemController.getAllItems)
    .post(itemController.uploadItemPhoto)
    .post(itemController.createItems)

router
    .route('/:id')
    .get(itemController.getItems)
    .patch(itemController.updateItems)
    .delete(itemController.deleteItems)

module.exports = router