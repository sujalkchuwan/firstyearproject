const express = require('express')
const router = express.Router()
const viewsController = require('./../Controllers/viewController')
const { route } = require('./itemRoutes')

router.get('/', viewsController.getadminHome)
router.get('/login', viewsController.getLoginForm)
router.get('/signup', viewsController.getSignupForm)
router.get('/adminlogin', viewsController.getadminLoginForm)
router.get('/itemForm', viewsController.getitemForm)
router.get('/userDBoard', viewsController.getUserHome)
router.get('/NotificationConfirm', viewsController.getConfirmNotificationPage)
router.get('/NotificationConfirm', viewsController.getAdminLog)


// router.get('/adminDashboard',viewsController.getadminHome)
module.exports = router