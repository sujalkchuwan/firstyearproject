const express = require('express')
const notifyController = require('./../Controllers/notifyController')

const router = express.Router()

router
    .route('/')
    .get(notifyController.getAllNotification)
    .post(notifyController.createNotification)

router
    .route('/:id')
    .get(notifyController.getNotification)
    .delete(notifyController.deleteNotification)
    .patch(notifyController.updateNotification)

module.exports = router