const express = require('express')
const userController = require('./../Controllers/userController')
const authController = require('./../Controllers/authController')
const admin_authController = require('./../Controllers/admin_authController')

const router = express.Router()

router.post('/signup', authController.signup)
router.post('/login', authController.login)
router.post('/adminlogin', admin_authController.login)
router.get('/logout',authController.logout)

router
    .route('/')
    .get(userController.getAllUsers)
    .post(userController.createUser)

router
    .route('/:id')
    .get(userController.getUser)
    .patch(userController.uploadUserPhoto)
    .patch(userController.updateMe)
    .delete(userController.deleteUser)


// router.patch(
//     '/updateMe',
//     userController.uploadUserPhoto,
//     userController.updateMe)

module.exports = router