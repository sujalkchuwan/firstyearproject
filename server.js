const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path: './config.env'})
const app = require('./app')

const DB = process.env.DATABASE.replace('PASSWORD',
    process.env.DATABASE_PASSWORD,
    )
//const DB = process.env.DATABASE
//To store the data in local
// const local_DB = process.env.DATABASE_LOCAL
// mongoose.connect(local_DB).then((con)=>
// {
//     console.log(con.connections)
//     console.log('DB connection succesful')
// }).catch(error =>console.log(error));
mongoose.set("strictQuery", false);
mongoose.connect(DB).then((con)=>{
    console.log('DB connection succesful')
}).catch(error =>console.log(error));

const port=4001
app.listen(port, ()=>{
    console.log(`Apprunning on port ${port}..`)})
