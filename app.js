const express = require("express")
const path = require("path")
const app = express()
app.use(express.json())

const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const itemRouter = require('./routes/itemRoutes')
const cartRouter = require('./routes/cartRoutes')
const notifyRouter = require('./routes/notifyRoutes')



app.use('/api/v1/users',userRouter)
app.use('/api/v1/itemForm',itemRouter)
app.use('/api/v1/cart',cartRouter)
app.use('/api/v1/notify',notifyRouter)

app.use('/', viewRouter)

app.use(express.static(path.join(__dirname, 'views')))
app.use(express.static( 'users'));

module.exports=app;
