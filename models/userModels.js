const mongoose = require ('mongoose')
const validator = require('validator')
const bcrypt = require("bcryptjs")
const validate = require('mongoose-validator')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please tell us your name!' ],
    },

    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true
    },

    PhoneNumber: {
        type: Number,
        required:[true, 'Please provide your phone number'],
        minlength: 8
    },

    Occupation: {
        type: String,
        enum: ['Staff','Student'],
        default: 'Student',
    },

    password: {
        type: String,
        required: [true, 'Please provide a password!'],
        select: false,   
    },
    photo: {
        type: String,
        default:'profile.png'   
    },

    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
          validator: function (el) {
            return el === this.password;
          },
          message: 'Passwords are not the same!',
        },
      },

    active: {
        type: Boolean,
        default: true,
        select: false,
    },

})


// mongooose middleware
userSchema.pre('save',async function (next){
// Only run this function if password was actually modified
    if (!this.isModified('password'))return next()

    //Hash the password with cost of 12
    this.password = await bcrypt.hash(this.password, 12)

    //Delete passwordConfirm field
    this.passwordConfirm = undefined
    next()
})

userSchema.path('email').validate(function (email){
    var emialRegex = /^[0-9]{8}\.gcit\@rub\.edu\.bt$/g;
    return emialRegex.test(email);
}, 'Enter your vaild college email')

userSchema.path('PhoneNumber').validate(function (PhoneNumber){
    var phonenumberRegex = /^17[0-9]{6}|77[0-9]{6}$/g;
    return phonenumberRegex.test(PhoneNumber);
}, 'Enter your vaild phone number')

userSchema.path('password').validate(function (password){
    var passwordRegex = /[0-9a-zA-Z]{8,}/g;
    return passwordRegex.test(password);
}, 'Enter passsword more than 8')


//Instance method is available in all document of certain collection while login
userSchema.methods.correctPassword = async function(
    candidatePassword,//Password that user pass
    userPassword,//password that store in the database
){
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User', userSchema )
module.exports = User