const mongoose = require ('mongoose')

const itemSchema = new mongoose.Schema({
    photo: {
        type: String,
        required: [true, 'Please upload the image' ],
    },

    itemName: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true
    },

    price: {
        type: Number,
        required:[true, 'Please provide your price number'],
    },

    category: {
        type: String,
        enum: ['veg','non-veg','drinks'],
        default: 'veg',
        required:[true, 'Please provide Category'],
        
    },

    subCategory: {
        type: String,
        enum: ['drinks','meal','fastfood'],
        default: 'drinks',   
        required:[true, 'Please provide subCategory'],
    },

    active: {
        type: Boolean,
        default: true,
        select: false,
    },
})

const Item = mongoose.model('Item', itemSchema )
module.exports = Item