const mongoose = require ('mongoose')
//const validator = require('validator')
//const bcrypt = require("bcryptjs")
//const validate = require('mongoose-validator')

const adminSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true
    },

    password: {
        type: String,
        required: [true, 'Please provide a password!'],  
    }
})

//Instance method is available in all document of certain collection while login


// adminSchema.methods.correctPassword = async function(
//     adminSchema.password,//Password that user pass
//     userPassword,//password that store in the database
// ){
//      return await compare(candidatePassword, userPassword)
// }

const Admin = mongoose.model('Admin', adminSchema )
module.exports = Admin