const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({

    photo:{
        type: String,
        required: [true, 'A tour must have a cover image'],
    },


    itemName:{
        type:String,
        required: [true, 'Item Name'],
    },
    price: {
        type: Number,
        required: true,
        required: [true, 'Price'],

    },

    count: {
        type: Number,
        defualt: 1,
        required: [true, 'count number'],
    },
    
    checked:{
        type: String,
        default:'no'
    },
    
    //SME: Subject Matter Experts
    user:{
        type: String
    },
    category:{
        type:String,
        
    }
})

const Cart= mongoose.model('Cart', cartSchema)
module.exports= Cart