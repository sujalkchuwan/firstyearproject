const mongoose = require('mongoose')

const notifySchema = new mongoose.Schema({

    date:{
        type: Date,
        required: [true, 'provide date'],
    },

    user:{
        type: String,
        required: [true, 'provide user']
    },
    status:
    {
        type:String,
        required: [true, 'provide status'],
        default: "pending"
    }
})

const notification= mongoose.model('notification', notifySchema)
module.exports= notification