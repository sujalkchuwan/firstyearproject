import { showAlert } from './alert.js'
export const postingItems = async (data) => 
{ let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/itemForm';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/itemForm';
    }
    try {
        const res = await axios({
            method: 'POST',
            url: URL,
            data
        })

        console.log(res.data.status)
        if (res.data.status === 'success') 
        {
            showAlert('success', 'Data updated successfully!') 
        }
    } catch (err) {
        
        let message =
        typeof err.response !== 'undefined'
        ? err.response.data.message
        : err.message
        //showAlert('error', 'Error: Posting Error', message )
        showAlert('error', err.response.data.message)
    }
}
// function drink(){
//     var sc = document.querySelector('.user-input-box')
//     if(document.getElementById('category').value !== 'drinks'){
//         sc.innerHTML = `<label for="sub-category">SUB-CATEGORY</label>
//         <select class="form-select" id="sub-category" aria-label="Floating label select example">
//             <option value="fastfood" style="color: black;">Fast Food</option>
//             <option value="meal" style="color: black;">Meals</option>
//         </select>`
//     }
// }

const userDataForm = document.querySelector('.itempost-form')

userDataForm.addEventListener('submit',(e)=>
{
    e.preventDefault()
    const form = new FormData()
    form.append('photo', document.getElementById('photo').files[0])
    form.append('itemName', document.getElementById('itemName').value)
    form.append('price', document.getElementById('price').value)
    form.append('category', document.getElementById('category').value)
    form.append('subCategory', document.getElementById('sub-category').value)
    console.log("this is itemform: ",form);
    
    postingItems(form)

})

