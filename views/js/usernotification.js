import { showAlert } from "./alert.js";

const allCarts = async()=> {
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/notify';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/notify';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        displayCarts(res.data)
        
    } catch(err){
        showAlert.console.log(err)
    }
}

allCarts()

const displayCarts = (image) => {
    var arr = image.data
    for (let i=0; i<arr.length;i++){
        var card = document.querySelector('.box').cloneNode(true)
        
        var el2 = card.querySelector('.card__sub-heading')
        var el3 = card.querySelector('.sub_heading')
        
       
        const element = arr[i]
        console.log(element)
        if(element.status === 'pending'){continue}
    
        el2.innerHTML = 'status: '+element.status
        el3.innerHTML = 'date: ' + element.date
       
        var card2 = document.querySelector('.box')

        card2.insertAdjacentElement('afterend', card)
    }
    document.querySelector('.box').remove()
}




