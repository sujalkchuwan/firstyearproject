import { showAlert } from "./alert.js";

export const signup = async(name,email,Occupation,PhoneNumber,password,passwordConfirm) =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/users/signup';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/users/signup';
    }
    try{
        const res=await axios({        
            method:'POST',
            url:URL,
            data:{
                name,
                email,
                Occupation,
                PhoneNumber,
                password,
                passwordConfirm
            },
        })
        if(res.data.status ==='success'){
            showAlert('success','Account created successfully!')
            window.setTimeout(()=> {
                location.assign('/userDBoard')
            }, 1500)
        }

    }catch(err){
        console.log(err)

        let message = 
            typeof err.response !=='undefined'
            ?err.response.data.message
            :err.message
            
        console.log(err)
        
        showAlert('error', message)

    }
}

document.querySelector('.signup-form').addEventListener('submit', (e)=> 
{
    e.preventDefault()
    const email = document.getElementById('email').value
    const name = document.getElementById('fullName').value
    const occupation = document.getElementById('occupation').value
    const phonenumber = document.getElementById('phoneNumber').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('cpassword').value
    
   
    // const occupation = document.querySelector('input[name=role]:checked').value

    signup(name,email,occupation,parseInt(phonenumber),password,passwordConfirm)
})