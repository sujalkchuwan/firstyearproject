import { showAlert } from "./alert.js";
var obj = JSON.parse(document.cookie.substring(6))
var allItem
const allCarts = async()=> {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/cart';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/cart';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        allItem = res.data
        displayCarts(res.data)
        
    } catch(err){
        showAlert.console.log(err)
    }
}

allCarts()

const displayCarts = (image) => {
    var arr = image.data
    var TAmount = 0
    
    for (let i=0; i<arr.length;i++){
        var card = document.querySelector('.box').cloneNode(true)
        var el1 = card.querySelector('.card__picture-img')
        var el2 = card.querySelector('.card__sub-heading')
        var el3 = card.querySelector('.card__text')
        var el4 = card.querySelector('.total-count')
    
        var deleteItem = card.querySelector('.btn-area')
        var incrementCount = card.querySelector('.increment-count')
        var decrementCount = card.querySelector('.decrement-count')

        const element = arr[i]
        if(element.checked === 'yes' || element.user !== obj._id){continue}
        el1.src = 'img/itemPhoto/'+element.photo
        el2.innerHTML = ''+element.itemName
        el3.innerHTML = ''+element.price
        el4.innerHTML=''+element.count

        TAmount += element.price*element.count
    
        deleteItem.addEventListener('click', callit(element._id))

        incrementCount.addEventListener('click', async e =>{
            let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/cart/'+element._id;
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/cart/'+element._id;
    }
            try{
                var count = element.count+1
                
                const res =  await axios({
                    method: 'PATCH',
                    url:URL,
                    data:{count}
                }) 
                if(res.data.status==='success'){
                    location.reload(true)
                }
            } catch(err){
                showAlert.console.log("this is error: ",err)
            }
        })

        decrementCount.addEventListener('click', async e =>{
            let URL;
            if (window.location.hostname === 'localhost') {
                // console.log('This is localhost')
                URL = 'http://localhost:4001/api/v1/cart/'+element._id;
            } else {
                URL = 'https://grabandgo.onrender.com/api/v1/cart/'+element._id;
            }
            
            try{
                var count = element.count-1
                const res =  await axios({
                    method: 'PATCH',
                    url:URL,
                    data:{count}
                })
                if(res.data.status==='success'){
                    location.reload(true)
                }
            } catch(err){
                showAlert.console.log("this is error: ",err)
            }
        })

        var card2 = document.querySelector('.box')

        card2.insertAdjacentElement('afterend', card)
    }
    document.querySelector('.box').remove()

    var el5 = document.querySelector('.totalAmount')
    el5.innerHTML=''+TAmount
}


const callit = (someVar) => {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL ='http://localhost:4001/api/v1/cart/'+someVar;
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/cart/'+someVar;
    }
    
    return (e) => {
        try{
            const res =  axios({
                method: 'DELETE',
                url:URL,
            })
        } catch(err){
            showAlert.console.log("this is error: ",err)
        }
    }
}

document.querySelector('.confirmOrder').addEventListener('click', async e =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL ='http://localhost:4001/api/v1/notify';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/notify';
    }

    try{
        e.preventDefault()
        const date = document.getElementById('orderdate').value
        const user = obj._id

        const res =  await axios({
            method: 'POST',
            url:URL,
            data:{
                date,
                user
            }
        }) 
        if(res.data.status==='success'){
            
            var arr2 = allItem.data
            const checked = 'yes'

            for (let i=0; i<arr2.length;i++){
                let URL;
                if (window.location.hostname === 'localhost') {
                    // console.log('This is localhost')
                    URL ='http://localhost:4001/api/v1/cart/'+arr2[i]._id;
                } else {
                    URL = 'https://grabandgo.onrender.com/api/v1/cart/'+arr2[i]._id;
                }
                const res =  axios({
                    method: 'PATCH',
                    url:URL,
                    data:{
                        checked
                    }
                })
            }
            
        }

        
    } catch(err){
        showAlert.console.log("this is error: ",err)
    }
})
