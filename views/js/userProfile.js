import { showAlert } from "./alert.js";
var obj = JSON.parse(document.cookie.substring(6))


export const getProfile = async (data, type) => 
{   let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = "http://localhost:4001/api/v1/users/";
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/users/';
    }
    try {
        const res = await axios({
            method: 'GET',
            url:URL+obj._id,
        })

        
        if(res.data.status === 'success'){
    
            var el1 = document.querySelector('form.form-user-data')
            el1.innerHTML =`<div>
            <div class="profile-pic-div">
                <input class="form__upload" type="file" accept="image/*" id="photo" name="photo" hidden />
            
                <label for="photo"><img src="../img/userPhoto/` + res.data.data.photo + `" alt="User photo" id="image"></label>
            </div>

            <div class="form_group" id="grouptag"> <label class="name" for="name">Name</label> <input class="form_input" id="name" type="text" value="` +
            res.data.data.name.toUpperCase() +
            `" required="required" name="name"/><br><br>
            <button class="btn btn--small btn--green">Update</button></div>
            </div>`

        }
        document.getElementById("image").style.width = "200px";
        document.getElementById("image").style.marginLeft = "40%";
        document.getElementById("image").style.borderRadius = "10%";
        document.getElementById("grouptag").style.marginLeft = "40%";

        
    } catch (err) {
        let message =
        typeof err.response !== 'undefined'
        ? err.response.data.message
        : err.message
        showAlert('error', err.response.data.message)
    }
}
getProfile()

export const updateSettings = async (data) => 
  {
      try {
          const res = await axios({
              method: 'PATCH',
              url:"http://localhost:4001/api/v1/users/"+obj._id,
              data,
          })
          
          if(res.data.status === 'success'){
            location.reload()
          }
          
      } catch (err) {
          let message =
          typeof err.response !== 'undefined'
          ? err.response.data.message
          : err.message
          showAlert('error', err.response.data.message)
      }
  }
  
const userDataForm = document.querySelector('.form.form-user-data')
  userDataForm.addEventListener('submit', (e) => {
      e.preventDefault()
      const form = new FormData()
      form.append('name', document.getElementById('name').value)
      form.append('photo', document.getElementById('photo').files[0])
      
      updateSettings(form) 
  })
