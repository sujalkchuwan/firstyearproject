import { showAlert } from "./alert.js";

const allCarts = async()=> {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/cart';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/cart';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        displayCarts(res.data)
        
    } catch(err){
        showAlert.console.log(err)
    }
}

allCarts()

const displayCarts = (image) => {
    var arr = image.data
    
    for (let i=0; i<arr.length;i++){
        var card = document.querySelector('.displycardbox').cloneNode(true)
        var el1 = card.querySelector('.itemName')
        var el2 = card.querySelector('.quantity')
        var el3 = card.querySelector('.category')

        const element = arr[i]
        if(localStorage.getItem('userID') !== element.user) {continue}
        el1.innerHTML = ''+element.itemName
        el2.innerHTML = ''+element.count
        el3.innerHTML = ''+element.category

        var card2 = document.querySelector('.displycardbox')

        card2.insertAdjacentElement('afterend', card)
    }
    document.querySelector('.displycardbox').remove()
}

var Reject = document.querySelector('.reject')
var Confirm = document.querySelector('.confirm')

Reject.addEventListener('click', async e =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/notify/'+localStorage.getItem('notificationID');
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/notify/'+localStorage.getItem('notificationID');
    }
    try{
        var status = "reject"
        const res =  await axios({
            method: 'PATCH',
            url:URL,
            data:{status}
        }) 
        if(res.data.status==='success'){
            location.reload(true)
        }
    } catch(err){
        showAlert.console.log("this is error: ",err)
    }
})

Confirm.addEventListener('click', async e =>{
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/notify/'+localStorage.getItem('notificationID');
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/notify/'+localStorage.getItem('notificationID');
    }
    try{
        var status = "confirm"
        const res =  await axios({
            method: 'PATCH',
            url:URL,
            data:{status}
        }) 

        if(res.data.status==='success'){
            location.reload(true)
        }
    } catch(err){
        showAlert.console.log("this is error: ",err)
    }
})
