import { showAlert } from "./alert.js";

const allCarts = async()=> {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/notify';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/notify';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        displayCarts(res.data)
        
    } catch(err){
        showAlert.console.log(err)
    }
}

allCarts()

const date = new Date();

const day = date.getDate();
const month = date.getMonth() + 1;
const year = date.getFullYear();

const displayCarts = async (image) => {
    var arr = image.data
    for (let i=0; i<arr.length;i++){
        var card = document.querySelector('.box').cloneNode(true)
        
        var el2 = card.querySelector('.card__sub-heading')
        var el3 = card.querySelector('.user-name')
        
        const element = arr[i]
        const sp1 = element.date.split("T", 3)
        const arrDate = sp1[0].split("-")

        if(Number(arrDate[0]) >= year && Number(arrDate[1])>= month && Number(arrDate[2])>=day){continue}
        let URL
        if (window.location.hostname === 'localhost') {
            // console.log('This is localhost')
            URL = 'http://localhost:4001/api/v1/users/'+element.user;
        } else {
            URL = 'https://grabandgo.onrender.com/api/v1/users/'+element.user;
        }
        try{
            const res = await axios({
                method: 'GET',
                url:URL,
            })
            el2.innerHTML = 'Email: '+res.data.data.email
            el3.innerHTML = 'Name: '+res.data.data.name
            
        } catch(err){
            showAlert.console.log(err)
        }

        var card2 = document.querySelector('.box')

        card2.insertAdjacentElement('afterend', card)
    }
    document.querySelector('.box').remove()
}




