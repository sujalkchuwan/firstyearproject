const logout = async()=> {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/users/logout';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/users/logout';
    }

    try{
        const res = await axios({
            method: 'GET',
            url: URL,
        })
        if(res.data.status === 'success'){
            window.setTimeout(()=>{
                location.assign('/login')
            })          
}
}catch(err){
    showAlert('error', 'Error logging out! Try again.')
}
}

var doc = document.querySelector('#logout')
doc.addEventListener('click', (e)=> logout())