import { showAlert } from "./alert.js";

const allCarts = async()=> {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/notify';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/notify';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })

        displayCarts(res.data)
        
    } catch(err){
        showAlert.console.log(err)
    }
}

allCarts()

const displayCarts = (notification) => {
    var arr = notification.data
    
    for (let i=0; i<arr.length;i++){
        var card = document.querySelector('.cardbox').cloneNode(true)
        var el2 = card.querySelector('.card__sub-heading')
        var el3 = card.querySelector('.card__text')

        const element = arr[i]

        el2.innerHTML = 'User Name: '+element.user
        el3.innerHTML = 'Date of recieption: '+element.date
        
        card.addEventListener('click', async e =>{
            
            window.setTimeout(()=>{
                localStorage.setItem("notificationID", element._id)
                localStorage.setItem("userID", element.user)
                location.assign('/NotificationConfirm',)
            }, 500)
        })

        var card2 = document.querySelector('.cardbox')

        card2.insertAdjacentElement('afterend', card)
    }
    document.querySelector('.cardbox').remove()

}
const logout = async()=> {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/users/logout';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/users/logout';
    }
    try{
        const res = await axios({
            method: 'GET',
            url: URL,
        })
        if(res.data.status === 'success'){
            window.setTimeout(()=>{
                location.assign('/Adminlogin')
            })          
}
}catch(err){
    showAlert('error', 'Error logging out! Try again.')
}
}

var doc = document.querySelector('#logout')
doc.addEventListener('click', (e)=> logout())