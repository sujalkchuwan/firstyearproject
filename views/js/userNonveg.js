import { showAlert } from "./alert.js";
var obj = JSON.parse(document.cookie.substring(6))

const imagePost = async()=> {
    let URL;
    if (window.location.hostname === 'localhost') {
        // console.log('This is localhost')
        URL = 'http://localhost:4001/api/v1/itemForm';
    } else {
        URL = 'https://grabandgo.onrender.com/api/v1/itemForm';
    }
    try{
        const res = await axios({
            method: 'GET',
            url:URL,
        })
        
        displayImage(res.data)
    } catch(err){
        showAlert.console.log("this is error: ",err)
        
    }
}

imagePost()

const displayImage = (image) => {
    var arr = image.data
    for (let i=0; i<arr.length;i++){

        const element = arr[i]
        if(element.category === 'non-veg')
        {
            if(element.subCategory == 'fastfood'){
                c1(element)
            }
            else{
                c2(element)
            }

        }
    }
    document.querySelector('.card1').remove()
    document.querySelector('.card2').remove()
}


function c1(element){
    var card = document.querySelector('.card1').cloneNode(true)
    card = addItemContent(card, element)
    var card1 = document.querySelector('.card1')
    card1.insertAdjacentElement('afterend', card)
}

function c2(element){
    var card = document.querySelector('.card2').cloneNode(true)
    card = addItemContent(card, element)
    var card2 = document.querySelector('.card2')
    card2.insertAdjacentElement('afterend', card)
}


function addItemContent(card, element){
    
    var el1 = card.querySelector('.card__picture-img')

    var el2 = card.querySelector('.card__sub-heading')
    var el3 = card.querySelector('.card__text')
    
    el1.src = 'img/itemPhoto/'+element.photo
    el2.innerHTML = 'ItemName: '+element.itemName
    el3.innerHTML = 'Nu.'+element.price
    var deleteItem = card.querySelector('.addTocard')
    deleteItem.addEventListener('click',callItemR(element.photo, element.itemName, element.price, 1, obj._id,element.category))

    return card
}

const callItemR = (photo, itemName, price, count, user, category) => {

    return (e) => {
        let URL;
        if (window.location.hostname === 'localhost') {
            // console.log('This is localhost')
            URL = 'http://localhost:4001/api/v1/cart';
        } else {
            URL = 'https://grabandgo.onrender.com/api/v1/cart';
        }
        try{
            const res =  axios({
                method: 'POST',
                url:URL,
                data:{
                    photo,
                    itemName,
                    price,
                    count,
                    user,
                    category
                },
                
            })
        } catch(err){
            showAlert.console.log("this is error: ",err)
        }
    }
}
const logout = async()=> {
    try{
        const res = await axios({
            method: 'GET',
            url: 'http://localhost:4001/api/v1/users/logout',
        })
        if(res.data.status === 'success'){
            window.setTimeout(()=>{
                location.assign('/login')
            })          
}
}catch(err){
    showAlert('error', 'Error logging out! Try again.')
}
}

var doc = document.querySelector('#logout')
doc.addEventListener('click', (e)=> logout())