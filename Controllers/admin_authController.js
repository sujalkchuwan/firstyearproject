const Admin = require('../models/adminModels')
const jwt = require('jsonwebtoken')
const AppError = require('../utils/appError')

const signToken = (id) => {
    return jwt.sign({id},
        process.env.JWT_SECRET,{
            expiresIn:process.env.JWT_EXPIRES_IN,
        })
}
const createSendToken = (admin, statusCode, res) =>
{
    const token =signToken(admin._id)
    const cookieOptions = {
        expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000, 
        ),
        httpOnly: true,
    }
    res.cookie('jwt',token, cookieOptions)
    res.status(statusCode).json({
        status:"success",
        token,
        data : {
            admin
        }
    })
}

exports.login = async(req, res, next)=>
{
    try{
        const {email, password} = req.body
        //1) check if email and password exist
        if(!email || !password)
        {
            return next(new AppError('Please provide an email and password!', 400))
        }

        //2) Check if user exists && password is correct

        const admin = await Admin.findOne({email}).select('+password')
        
        //const correct = await user.correctPassword(password, user.password)

        if(!admin || !(admin.password===password)){
            return next(new AppError('Incorrect email or password', 401))
        }

        // if(!admin || !(await admin.correctPassword(admin.password)))
        // {
        //     return next(new AppError('Incorrect email or password', 401))
        // }
        //3) If everything ok, send token to client
        createSendToken(admin, 200, res)
        // const token = signToken(user._id)
        // res.status(200).json({
        //     status: 'success',
        //     token,
        // })
    }
    catch(err)
    {
        res.status(500).json({error: err.message});

    }
}
