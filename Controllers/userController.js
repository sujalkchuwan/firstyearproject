const User = require('./../models/userModels')
const multer = require('multer')

exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.find()
        res.status(200).json({data:users, status: 'success'})
    }catch (err) {
        res.status(500).json({ error: err.message});
    }
}

exports.createUser = async (req, res) => {
    try{
        const user = await User.create(req.body);
        res.json({data: user, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.getUser = async (req, res) => {
    try{
        const user = await User.findById(req.params.id);
        res.json({data: user, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.updateUser = async (req, res) => {
    try{
        const user = await User.findByIdAndUpdate(req.params.id, req.body);
        res.json({data: user, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.deleteUser = async (req, res) => {
    try{
        const user = await User.findByIdAndDelete(req.params.id);
        res.json({data: user, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

const filterObj = (obj, ...allowesFields) => {
    const newObj = {}
    Object.keys(obj).forEach((el) =>{
        if(allowesFields.includes(el)) newObj[el] = obj[el]
    })
    return newObj
}

exports.updateMe = async(req, res, next) => {
    try{
        //2 Filtered out unwanted fields names that are not allowed to be updated
        const filteredBody = filterObj(req.body, 'name','photo')

        if(req.body.photo !== 'undefined'){
            filteredBody.photo = req.file.filename
        }
        // var obj = JSON.parse(req.cookies.token)
        const updatedUser = await User.findByIdAndUpdate(req.params.id, filteredBody,
            {
                new: true,
                runValidators: true,
            })
            res.status(200).json({
                status: 'success',
                data: {user: updatedUser},
            })
    }
    catch(err)
    {
        res.status(500).json({error: err.message})
    }
}

const multerStorage = multer.diskStorage({
    
    destination:(req, file, cb) => {
        cb(null, 'views/img/userPhoto')
    },
    filename: async(req, file, cb)=>{

    const ext = file.mimetype.split('/')[1]
    cb(null, `item-${"image"}-${Date.now()}.${ext}`)
    },
})

const multerFilter = (req, file, cb) => {
    if(file.mimetype.startsWith('image')){
        cb(null, true)
    }
    else{
        cb(new AppError('Not an image! Please upload only images', 400), false)
    }
}

const upload = multer({
    storage: multerStorage,
    fileFilter: multerFilter,
})

exports.uploadUserPhoto = upload.single('photo')



