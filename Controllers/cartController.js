const carts = require('./../models/cartModel')
const jwt = require('jsonwebtoken')
const User = require('./../models/userModels')
const promisify = require('util.promisify')
const { db } = require('./../models/cartModel')

exports.getAllCart = async (req, res) => {
    try {

    const carts1 = await carts.find()
    res.status(200).json({data:carts1,status: 'success'})

  } catch (err) {
    res.status(500).json({ error: err.message});
  }
}
 
exports.createCart = async (req, res) => {
  try{
    //req.user = currentUserId(req)

    const NewCart = await carts.create(req.body)
    res.json({data: NewCart, status: "success"});

  }catch (err){
      res.status(500).json({error: err.message});
  }
}
 
exports.updateCart = async (req, res) => {
  try {
    const carts1 = await carts.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    })
 
    res.status(200).json({
      status: 'success',
      data: {
        carts1,
      },
    })
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err,
    })
  }
}
 
exports.deleteCart = async (req, res) => {
  try {
    const carts1 = await carts.findByIdAndDelete(req.params.id)
 
    res.status(200).json({
      status: 'success',
      data: {
        carts1,
      },
    })
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err,
    })
  }
}

exports.deleteAllCart = async (req, res) => {
    
    db.collections.carts.deleteMany({ user: req.params.id }).then(function(){

        res.status(200).json({
            status: 'success',
        })

    }).catch(function(error){
        
        res.status(404).json({
            status: 'fail',
            message: error,
        })
    });
  }

