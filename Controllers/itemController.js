const items = require('./../models/itemModels')
const multer = require('multer')
const jwt = require('jsonwebtoken')

exports.getAllItems = async (req, res, next) => {
    try {
        const item = await items.find()
        res.status(200).json({data:item, status: 'success'})
    }catch (err) {
        res.status(500).json({ error: err.message});
    }
}

exports.createItems = async (req, res) => {
    try{
        // console.log(req.headers)
        req.body.photo = req.file.filename
        const item = await items.create(req.body);
        res.json({data: item, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.getItems = async (req, res) => {
    try{
        const item = await items.findById(req.params.id);
        res.json({data: item, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.updateItems = async (req, res) => {
    try{
        const item = await items.findByIdAndUpdate(req.params.id, req.body);
        res.json({data: item, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.deleteItems = async (req, res) => {
    try{
        const item = await items.findByIdAndDelete(req.params.id);
        res.json({data: item, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

const multerStorage = multer.diskStorage({
    
    destination:(req, file, cb) => {
        cb(null, 'views/img/itemPhoto')
    },
    filename: async(req, file, cb)=>{

        //user-id-currentimestamp.extension
        const ext = file.mimetype.split('/')[1]
        cb(null, `user-${"decoded.id"}-${Date.now()}.${ext}`)
    },
})
const multerFilter = (req, file, cb) => {
    if(file.mimetype.startsWith('image')){
        cb(null, true)
    }
    else{
        cb(new AppError('Not an image! Please upload only images', 400), false)
    }
}

const upload = multer({
    storage: multerStorage,
    fileFilter: multerFilter
})

exports.uploadItemPhoto = upload.single('photo')
