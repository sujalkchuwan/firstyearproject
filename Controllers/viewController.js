const path = require('path')
/* LogIn PAGE */

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
}

/* SIGNUP PAGE */

exports.getSignupForm = (req, res)=> {
    res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
}

/*HOME PAGE */

// exports.getHome = (req, res) => {
//     res.sendFile(path.join(__dirname, '../', 'views', 'dashboard.html'))
// }

/*Admin login*/

exports.getadminLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'Adminlogin.html'))
}

/*Admin dashboard*/
exports.getadminHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'admindashboard.html'))}

    /* ItemForm PAGE */
exports.getitemForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'itemForm.html'))
}

/* USERS PAGE */
exports.getUserHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'userDashboard.html'))
}

exports.getConfirmNotificationPage= (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'NotificationConfirm.html'))
}

exports.getAdminLog= (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'adminLog.html'))
}


