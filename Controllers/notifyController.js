const notifications = require('./../models/notificationModel')
const jwt = require('jsonwebtoken')

exports.getAllNotification = async (req, res, next) => {
    try {
        const notification = await notifications.find()
        res.status(200).json({data:notification, status: 'success'})
    }catch (err) {
        res.status(500).json({ error: err.message});
    }
}

exports.createNotification = async (req, res) => {
    try{
        // console.log(req.headers)
        const notification = await notifications.create(req.body);
        res.json({data: notification, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.getNotification = async (req, res) => {
    try{
        const notification = await notifications.findById(req.params.id);
        res.json({data: notification, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.updateNotification = async (req, res) => {
    try{
        const notification = await notifications.findByIdAndUpdate(req.params.id, req.body);
        res.json({data: notification, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

exports.deleteNotification = async (req, res) => {
    try{
        const notification = await notifications.findByIdAndDelete(req.params.id);
        res.json({data: notification, status: "success"});
    }catch (err){
        res.status(500).json({error: err.message});
    }
}

